from django.apps import AppConfig


class BackendConfig(AppConfig):
    name = 'justapp.backend'
    verbose_name = u"Backend App"
