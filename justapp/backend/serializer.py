from django.urls import reverse
from rest_framework import serializers

from justapp.backend.models import Page, ContentType, PageContent, ContentAttribute


class PagesSerializer(serializers.ModelSerializer):
    """
    Сериализатор для списка всех страниц
    """

    url = serializers.SerializerMethodField()

    class Meta:
        fields = ('url', 'title')
        model = Page

    def get_url(self, page):
        return reverse('backend:page-detail', args=[page.id])


class ContentAttributesSerializer(serializers.ModelSerializer):
    """
    Сериализатор доп атрибутов контента
    """

    class Meta:
        model = ContentAttribute
        fields = ('name', 'value')


class ContentSerializer(serializers.ModelSerializer):
    """
    Сериализатор для объекта контента
    """

    attributes = ContentAttributesSerializer(many=True)

    class Meta:
        model = ContentType
        fields = ('title', 'counter', 'attributes')


class PageContentSerializer(serializers.ModelSerializer):
    """
    Сериализатор для объекта контента на странице
    """

    content = ContentSerializer(many=False)

    class Meta:
        fields = ('content',)
        model = PageContent


class PageDetailSerializer(serializers.ModelSerializer):
    """
    Сериализатор для деталей страницы
    """
    contents = PageContentSerializer(many=True)

    class Meta:
        fields = ('title', 'contents')
        model = Page
