from django.conf.urls import url

from justapp.backend.views import ApiPages

app_name = 'backend'


urlpatterns = (
    url(r'^v1/pages/$', ApiPages.as_view({'get': 'list'}), name='pages'),
    url(r'^v1/pages/(?P<page_id>\d+)/$', ApiPages.as_view({'get': 'retrieve'}), name='page-detail'),

)
