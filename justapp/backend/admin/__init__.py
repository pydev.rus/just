from django.contrib import admin

from justapp.backend.admin.models import PageContentAdmin, PageAdmin, ContentAdmin
from justapp.backend.models import Page, Text, Audio, Video, ContentType

try:
    admin.site.register(Page, PageAdmin)
    admin.site.register(Text, ContentAdmin)
    admin.site.register(Audio, ContentAdmin)
    admin.site.register(Video, ContentAdmin)
    #admin.site.register(ContentType)
finally:
    pass
