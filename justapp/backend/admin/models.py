from django.contrib.admin import ModelAdmin, StackedInline

from justapp.backend.models import ContentAttribute, ContentType, PageContent


class ContentAttributeInline(StackedInline):
    model = ContentAttribute
    extra = 0


class PageContentAdmin(StackedInline):
    model = PageContent
    extra = 0


class ContentAdmin(ModelAdmin):
    model = ContentType
    inlines = [ContentAttributeInline,]
    search_fields = ['title']


class PageAdmin(ModelAdmin):
    inlines = [PageContentAdmin]
    list_display = ('title', 'contents')
    search_fields = ('title', 'contents__content__title')

    def contents(self, obj):
        return [x.content.title for x in obj.contents.all()]