from rest_framework import status
from rest_framework.response import Response


class ResponseMixin(object):
    """ Mixin response """

    def prepare_error_response(self, error_title=None, error_list=None, status_code=status.HTTP_400_BAD_REQUEST, content_type="application/json"):
        response = {'status': 'fail', 'error': u'Inner error, please contact with tech support', 'error_list': []}
        if error_title:
            response['error'] = error_title
        if error_list:
            response['error_list'] = error_list
        return Response(response, status=status_code, template_name=None, content_type=content_type)

    def prepare_success_response(self, data=None, extra=None, status_code=status.HTTP_200_OK, content_type="application/json"):
        response = {'data': []}
        if data:
            response['data'] = data
        if extra:
            response.update(extra)
        return Response(response, status=status_code, template_name=None, content_type=content_type)
