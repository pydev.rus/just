from django.db import models

from justapp.backend.manager import PageManager


def make_uploads_path(instance, source_filename):
    return u"uploads/{0}/{1}".format(instance.title, source_filename)


class ContentType(models.Model):
    """
    Базовый класс для типов контента
    """
    title = models.CharField(max_length=200, verbose_name=u'Заголов')
    counter = models.PositiveIntegerField(verbose_name='Счетчик', default=0)

    class Meta:
        verbose_name = u'Тип контента'
        verbose_name_plural = u'Все типы контента'

    def __str__(self):
        return self.title


class ContentAttribute(models.Model):
    """
    Доп атрибуты контанта

    """
    content_type = models.ForeignKey(to=ContentType, verbose_name=u'Контент', on_delete=models.CASCADE, related_name='attributes')
    name = models.CharField(max_length=100, verbose_name=u'Название')
    value = models.CharField(max_length=200, verbose_name=u'Значение')

    class Meta:
        verbose_name = u'Атрибут контента'
        verbose_name_plural = u'Атрибуты контента'

    def __str__(self):
        return '{0} : {1}'.format(self.content_type.title, self.name)


class Video(ContentType):
    """
    Тип видео контент
    """
    file = models.FileField(verbose_name=u'Видео файл', upload_to=make_uploads_path)
    format = models.CharField(max_length=100, verbose_name=u'Формат')

    class Meta:
        verbose_name = u'Видео контент'
        verbose_name_plural = u'Видео контент'


class Audio(ContentType):
    """
    Тип аудио контент
    """
    file = models.FileField(verbose_name=u'Аудио файл', upload_to=make_uploads_path)
    artist = models.CharField(verbose_name=u'Артист', max_length=100)

    class Meta:
        verbose_name = u'Аудио контент'
        verbose_name_plural = u'Аудио контент'


class Text(ContentType):
    """
    Тип текстовый контент
    """
    header = models.CharField(max_length=200, verbose_name=u'Текст')

    class Meta:
        verbose_name = u'Текстовый контент'
        verbose_name_plural = u'Текстовый контент'


class Page(models.Model):
    """
    Страницы
    """
    title = models.CharField(max_length=200, verbose_name=u'Заголовок')

    # for pagination
    objects = PageManager()

    class Meta:
        verbose_name = u'Страница'
        verbose_name_plural = u'Страницы'

    def __str__(self):
        return self.title


class PageContent(models.Model):
    """
    Связка страница - контент
    """
    page = models.ForeignKey(to=Page, verbose_name=u'Страница', on_delete=models.CASCADE, related_name='contents')
    content = models.ForeignKey(to=ContentType, verbose_name=u'Контент', on_delete=models.CASCADE)

    class Meta:
        verbose_name = u'Контент на странице'
        verbose_name_plural = u'Контент на странице'

    def __str__(self):
        return '{0}: {1}'.format(self.page.title, self.content.title)