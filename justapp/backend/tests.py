import json

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase


class AccountTests(APITestCase):
    fixtures = ['audio.json', 'video.json', 'text.json', 'content_type.json', 'page.json', 'page_content.json', 'attributes.json']

    def test_list_pages(self):
        """
        Тест получения всех страниц
        :return:
        """
        url = reverse('backend:pages')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['data'], [{'title': 'page 1', 'url': '/backend/v1/pages/1/'},
                                                 {'title': 'page 2', 'url': '/backend/v1/pages/2/'}])

    def test_pages_pagination(self):
        """
        Тест пагинации страниц
        :return:
        """
        url = reverse('backend:pages')
        response = self.client.get(url, {'limit': '1', 'offset': '0'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['data'], [{'title': 'page 1', 'url': '/backend/v1/pages/1/'}])

        response = self.client.get(url, {'limit': '1', 'offset': '1'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['data'], [{'title': 'page 2', 'url': '/backend/v1/pages/2/'}])

    def test_page_detail(self):
        """
        Тест на получении деталей страницы и контента на ней

        ps: тест без запуска celery
        :return:
        """
        # получим страницу
        url = reverse('backend:page-detail', args=[1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # проверим атрибут страницы
        self.assertEqual(response.data['data']['title'], 'page 1')

        # проверим контент и его доп атрибуты
        self.assertEqual(json.dumps(response.data['data']['contents']), json.dumps([
            {
                "content":
                {
                    "title": "audio content 1",
                    "counter": 0,
                    "attributes": [
                        {"name": "album 4", "value": "album 4"}]
                }
            },
            {
                "content":
                {
                    "title": "audio content 2",
                    "counter": 0,
                    "attributes": [{"name": "album 2", "value": "album 2"}]
                }
            },
            {
                "content":
                {
                    "title": "video content 1",
                    "counter": 0,
                    "attributes": [{"name": "format", "value": "mkv"}]
                }
            },
            {
                "content":
                    {"title": "text content 1",
                     "counter": 0,
                     "attributes": []
                     }
            },
            {
                "content":
                    {
                     "title": "text content 2",
                     "counter": 0,
                     "attributes": []
                    }
             }]))
