from django.db.models import QuerySet
from django.db import models


class PageQuerySet(QuerySet):

    def query_by_args(self, **kwargs):
        limit = int(kwargs.get('limit', None)[0]) if 'limit' in kwargs else 10
        offset = int(kwargs.get('offset', None)[0]) if 'offset' in kwargs else 0
        total = self.count()
        return {
            'items': self[offset:offset + limit],
            'total': total,
        }


class PageManager(models.Manager):
    def get_query_set(self, **kwargs):

        query_set = PageQuerySet(self.model, using=self._db, **kwargs)
        return query_set

    def query_by_args(self, **kwargs):
        return self.get_query_set().query_by_args(**kwargs)
