from rest_framework.viewsets import ModelViewSet

from justapp.backend.mixin import ResponseMixin
from justapp.backend.models import Page, PageContent
from justapp.backend.serializer import PagesSerializer, PageDetailSerializer
from justapp.backend.tasks import increment


class ApiPages(ModelViewSet, ResponseMixin):
    def retrieve(self, request, page_id):
        """
        Получение деталей одной страницы
        :param request:
        :param page_id:
        :return:
        """
        try:
            try:
                page = Page.objects.get(id=page_id)
            except Page.DoesNotExist:
                return self.prepare_error_response(error_title=u'Getting page',
                                                   error_list=[u'Page with id = {0} not found'.format(page_id)],
                                                   status_code=400)
            increment.delay(page_id)
            serializer = PageDetailSerializer(page, many=False)
            return self.prepare_success_response(data=serializer.data)
        except Exception as e:
            return self.prepare_error_response(status_code=500)

    def list(self, request):
        """
        Получение списка всех страниц
        :param request:
        :return:
        """
        try:
            pages = Page.objects.query_by_args(**request.query_params)
            serializer = PagesSerializer(pages['items'], many=True)
            return self.prepare_success_response(data=serializer.data, extra={'total': pages['total']})
        except Exception as e:
            return self.prepare_error_response(status_code=500)
