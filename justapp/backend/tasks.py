from django.db import transaction

from justapp.backend.models import Page
from justapp.celery import app


@app.task
def increment(page_id):
    page = Page.objects.get(id=page_id)
    for content in page.contents.all():
        with transaction.atomic(savepoint=False):
            content.content.counter += 1
            content.content.save()
    return True
