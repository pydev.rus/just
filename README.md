Для запуска тестов
docker-compose up autotests

Для запуска приложения
docker-compose up runserver

Для доступа к admin
user: demo
pass: asd123456


API:

    Список страниц (с возможностью пагинации):

        params:
            *limit: количество элементов на страницу
            *offset: смещение в выборке данных

        request:
            curl -i -X GET \
             'http://0.0.0.0:8000/backend/v1/pages/?limit=10&offset=0&format=json'

        response:
            {
                "data":[
                    {
                        "url": "/backend/v1/pages/1/",
                        "title": "page 1"
                    },
                    {
                        "url": "/backend/v1/pages/2/",
                        "title": "page 2"
                    }
                ],
                "total": 2
            }

    Детали страницы:

        request:
            curl -i -X GET \
                'http://0.0.0.0:8000/backend/v1/pages/1/?format=json'

        response:
            {
                "data":{
                    "title": "page 1",
                    "contents":[
                        {"content":{"title": "audio content 1", "counter": 0, "attributes":[{"name": "album 4",…},
                        {"content":{"title": "audio content 2", "counter": 0, "attributes":[{"name": "album 2",…},
                        {"content":{"title": "video content 1", "counter": 0, "attributes":[{"name": "format",…},
                        {"content":{"title": "text content 1", "counter": 0, "attributes":[]…},
                        {"content":{"title": "text content 2", "counter": 0, "attributes":[]…}
                    ]
                }
            }