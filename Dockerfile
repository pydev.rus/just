 FROM python:3.6
 ENV PYTHONUNBUFFERED 1
 RUN mkdir /code
 WORKDIR /just
 ADD requirements.txt /just/
 RUN pip install -r requirements.txt
 ADD . /just/
